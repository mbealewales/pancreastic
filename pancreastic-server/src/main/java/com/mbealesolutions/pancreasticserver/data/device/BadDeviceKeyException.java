package com.mbealesolutions.pancreasticserver.data.device;

public class BadDeviceKeyException extends Exception {
    public BadDeviceKeyException() {
        super();
    }
    public BadDeviceKeyException(String message) {
        super(message);
    }
}
