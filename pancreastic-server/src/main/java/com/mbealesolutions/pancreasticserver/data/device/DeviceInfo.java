package com.mbealesolutions.pancreasticserver.data.device;

import java.util.Objects;

public class DeviceInfo {
    private String info;

    public String getInfo() {
        return this.info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int hashCode() {
        return java.util.Objects.hash(super.hashCode(), info);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceInfo that = (DeviceInfo) o;
        return Objects.equals(info, that.info);
    }
}
