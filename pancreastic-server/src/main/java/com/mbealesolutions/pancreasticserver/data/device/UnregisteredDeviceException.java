package com.mbealesolutions.pancreasticserver.data.device;

public class UnregisteredDeviceException extends Exception {
    public UnregisteredDeviceException() {
        super();
    }
    public UnregisteredDeviceException(String message) {
        super(message);
    }
}
