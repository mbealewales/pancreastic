package com.mbealesolutions.pancreasticserver.data.device;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Scope(value="singleton")
@Component
public class DeviceRegistry {
    private Map<DeviceInfo, String> deviceToUsername = new HashMap<>();
    public String lookupDevice(DeviceInfo deviceInfo) throws UnregisteredDeviceException {

        if (deviceToUsername.containsKey(deviceInfo)) {
            return deviceToUsername.get(deviceInfo);
        }
        throw new UnregisteredDeviceException("Device not registered");
    }

    public void lookupRegistration(RegistrationInfo registrationInfo) throws BadDeviceKeyException, DeviceKeyExpiredException {

    }
}
