package com.mbealesolutions.pancreasticserver.data;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(value="singleton")
@Component
public class GlucoseReading {
    private String glucose;

    public String getGlucose() {
        return this.glucose;
    }

    public void setGlucose(String glucose) {
        this.glucose = glucose;
    }
}
