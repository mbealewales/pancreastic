package com.mbealesolutions.pancreasticserver.data.device;

public class DeviceKeyExpiredException extends Exception {
    public DeviceKeyExpiredException() {
        super();
    }
    public DeviceKeyExpiredException(String message) {
        super(message);
    }
}
