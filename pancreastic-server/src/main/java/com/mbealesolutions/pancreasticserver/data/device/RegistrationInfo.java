package com.mbealesolutions.pancreasticserver.data.device;

public class RegistrationInfo {
    private DeviceInfo deviceInfo;

    private String key;

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }
    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
