package com.mbealesolutions.pancreasticserver.controller.rest;

import com.mbealesolutions.pancreasticserver.data.device.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeviceController {

    @Autowired
    private DeviceRegistry deviceRegistry;

    @RequestMapping(value="/devicelogin", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity login(@RequestBody DeviceInfo deviceInfo) {

        try {
            deviceRegistry.lookupDevice(deviceInfo);
            return new ResponseEntity(HttpStatus.OK);
        } catch (UnregisteredDeviceException unregisteredDeviceException) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value="/deviceregister", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity register(@RequestBody RegistrationInfo registrationInfo) {

        try {
            deviceRegistry.lookupRegistration(registrationInfo);
            return new ResponseEntity(HttpStatus.OK);
        } catch (BadDeviceKeyException bke) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } catch (DeviceKeyExpiredException dkee) {
            return new ResponseEntity(HttpStatus.GONE);
        }
    }
}
