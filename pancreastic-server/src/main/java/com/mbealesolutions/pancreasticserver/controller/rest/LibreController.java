package com.mbealesolutions.pancreasticserver.controller.rest;

import com.mbealesolutions.pancreasticserver.data.GlucoseReading;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LibreController {

    @Autowired
    private GlucoseReading glucoseReading;

    @RequestMapping(value="/reading", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity newReading(@RequestBody GlucoseReading value) {
        glucoseReading.setGlucose(value.getGlucose());

        return new ResponseEntity(HttpStatus.OK);
    }
}
