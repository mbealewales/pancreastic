package com.mbealesolutions.pancreasticserver.controller.rest;

import com.mbealesolutions.pancreasticserver.data.GlucoseReading;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccessController {

    @Autowired
    private GlucoseReading glucoseReading;

    @RequestMapping(value="/read", method = RequestMethod.GET)
    public @ResponseBody GlucoseReading getGlucoseReading() {
        return glucoseReading;
    }
}
