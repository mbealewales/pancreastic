package com.mbealesolutions.pancreasticserver;

import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.apache.catalina.Context;

@SpringBootApplication
public class PancreasticServerApplication {

	@Bean
	public TomcatServletWebServerFactory tomcatEmbeddedServletContainerFactory() {
		final TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory() {
				@Override
			protected void postProcessContext(Context context) {
				SecurityConstraint securityConstraint = new SecurityConstraint();
				securityConstraint.setUserConstraint("CONFIDENTIAL");
				final SecurityCollection collection = new SecurityCollection();
				collection.addPattern("/*");
				securityConstraint.addCollection(collection);
				context.addConstraint(securityConstraint);
			}
		};
		factory.addAdditionalTomcatConnectors(this.createConnection());
		return factory;
	}

	private Connector createConnection() {
		final String protocol = "org.apache.coyote.http11.Http11NioProtocol";
		final Connector connector = new Connector(protocol);

		connector.setScheme("http");
		connector.setPort(80);
		connector.setRedirectPort(443);
		return connector;
	}

	public static void main(String[] args) {
		SpringApplication.run(PancreasticServerApplication.class, args);
	}
}
