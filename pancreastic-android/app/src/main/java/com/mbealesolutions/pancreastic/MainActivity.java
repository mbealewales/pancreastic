package com.mbealesolutions.pancreastic;

import android.nfc.NfcAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mbealesolutions.pancreastic.event.EventHandler;
import com.mbealesolutions.pancreastic.event.speech.SpeechHandler;
import com.mbealesolutions.pancreastic.event.speech.SpeechInitialisedListener;
import com.mbealesolutions.pancreastic.nfc.NfcHandler;
import com.mbealesolutions.pancreastic.rest.RestDeviceClient;
import com.mbealesolutions.pancreastic.R;
import com.mbealesolutions.pancreastic.rest.RestResultHandler;

import java.util.Map;

public class MainActivity extends AppCompatActivity  implements SpeechInitialisedListener {

    private NfcHandler nfcHandler;
    private EventHandler eventHandler;

    private RestDeviceClient restClient;

    private void handleIntent(Intent intent)
    {
        nfcHandler.connectToSensor(intent);
    }

    @Override
    public void speechInitialized(int status) {
        nfcHandler = new NfcHandler(NfcAdapter.getDefaultAdapter(this), eventHandler);
        handleIntent(getIntent());
    }

    private String getDeviceId() {
        return Build.SERIAL;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        eventHandler = new EventHandler(new SpeechHandler(this, this));

        restClient = new RestDeviceClient(getDeviceId(), this);

        restClient.deviceLogin(new RestResultHandler() {
            @Override
            public void response(int statusCode, Map<String, String> body) {
                final TextView txtView1 = findViewById(R.id.txt1);
                if (statusCode != 200)
                {
                    final TextView txtView2 = findViewById(R.id.txt2);
                    String text1 = txtView1.getContext().getString(R.string.deviceNotRegistered);
                    txtView1.setText(text1);
                    String text2 = txtView2.getContext().getString(R.string.notRegisteredHtml);
                    txtView2.setText(Html.fromHtml(text2));
                    txtView2.setMovementMethod(LinkMovementMethod.getInstance());

                    final TextView txtView3 = findViewById(R.id.txt3);
                    String text3 = txtView3.getContext().getString(R.string.enterKeyForDevice);
                    txtView3.setText(text3);

                    final Button registerButton = findViewById(R.id.registerButton);
                    registerButton.setVisibility(View.VISIBLE);
                    registerButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final EditText et = findViewById(R.id.key);
                            et.setVisibility(View.VISIBLE);

                            restClient.deviceRegister(et.getText().toString(), new RestResultHandler() {
                                @Override
                                public void response(int statusCode, Map<String, String> body) {
                                    Log.d("REST", "statusCode is " + statusCode);
                                }
                            });
                        }
                    });

                } else {
                    String text1 = txtView1.getContext().getString(R.string.deviceRegistered);
                    txtView1.setText(text1);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        Log.d("MB:Activity", "onDestroy");
        eventHandler.stop();
        if (nfcHandler != null) {
            nfcHandler.stop();
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        Log.d("MB:Activity", "onResume");
        super.onResume();

        if (nfcHandler != null) {
            nfcHandler.resume(this);
        }
    }

    @Override
    protected void onPause() {
        /**
         * Call this before onPause, otherwise an IllegalArgumentException is thrown as well.
         */
        Log.d("MB:Activity", "onPause");
        if (nfcHandler != null) {
            nfcHandler.pause(this);
        }

        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d("MB:Activity", "onNewIntent");
        /**
         * This method gets called, when a new Intent gets associated with the current activity instance.
         * Instead of creating a new activity, onNewIntent will be called. For more information have a look
         * at the documentation.
         *
         * In our case this method gets called, when the user attaches a Tag to the device.
         */
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
