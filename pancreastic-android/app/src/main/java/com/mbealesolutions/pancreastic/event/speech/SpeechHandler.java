package com.mbealesolutions.pancreastic.event.speech;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.mbealesolutions.pancreastic.event.Event;

/**
 * Created by Mike on 01/03/2018.
 */

public class SpeechHandler implements TextToSpeech.OnInitListener {
    private TextToSpeech textToSpeech;
    private int status;
    private Map<Event, String> speechMapping;
    private SpeechInitialisedListener speechInitialisedListener;

    public SpeechHandler(Context context, SpeechInitialisedListener initialisedListener) {
        textToSpeech = new TextToSpeech(context, this);
        speechInitialisedListener = initialisedListener;

        speechMapping = new HashMap<>();
        speechMapping.put(Event.DeviceDisabledNfc, "Please enable NFC on your device");
        speechMapping.put(Event.DeviceNoNfc, "Your device does not support NFC");
        speechMapping.put(Event.SensorConnected, "Sensor connected");
        speechMapping.put(Event.SensorConnectFailed, "Failed to connect sensor");
        speechMapping.put(Event.SensorDataReceived, "Your blood glucose reading is 5.2");
        speechMapping.put(Event.SensorDisconnected, "Sensor disconnected");
        speechMapping.put(Event.AppStarted, "Application Started");
    }
    @Override
    public void onInit(int status) {
        this.status = status;
        if (status == TextToSpeech.SUCCESS) {
            Log.d("MB:Speech", "onInit");
            int result = textToSpeech.setLanguage(Locale.UK);
            textToSpeech.setPitch(3);
            textToSpeech.setSpeechRate(2);
        }
        speechInitialisedListener.speechInitialized(status);
    }

    public void say(Event event) {
        if (status == TextToSpeech.SUCCESS) {
            final String toSay = speechMapping.get(event);
            Log.d("MB:Speech", toSay);
            textToSpeech.speak(toSay, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    public void stop() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }
}
