package com.mbealesolutions.pancreastic.rest;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.mbealesolutions.pancreastic.MainActivity;
import com.mbealesolutions.pancreastic.R;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by mbeale on 03/03/2018.
 */

public class RestDeviceClient {

    private String deviceId;
    private Activity activity;
    private SSLContext sslContext;

    public RestDeviceClient(String deviceId, Activity activity) {
        this.deviceId = deviceId;
        this.activity = activity;

        try {
            final KeyStore ksTrust = KeyStore.getInstance("BKS");
            final InputStream instream = activity.getResources().openRawResource(R.raw.panceastic);
            ksTrust.load(instream, "09Xabmm983Ppqv".toCharArray());

            // TrustManager decides which certificate authorities to use.
            final TrustManagerFactory tmf = TrustManagerFactory
                    .getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ksTrust);
            this.sslContext = SSLContext.getInstance("TLS");
            this.sslContext.init(null, tmf.getTrustManagers(), null);
        } catch (Exception ex) {
            Log.e("REST", ex.toString());
        }

    }

    private void sendRestRequestOnNewThread(final String url, final String method, final Map<String, String> requestBody,
                                            final RestResultHandler handler) {
        requestBody.put("device", this.deviceId);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final URL restEndpoint = new URL("https://mbealesolutions.com/" + url);

                    final HttpsURLConnection myConnection =
                            (HttpsURLConnection) restEndpoint.openConnection();

                    myConnection.setSSLSocketFactory(sslContext.getSocketFactory());
                    myConnection.setHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String s, SSLSession sslSession) {
                            return "mbealesolutions.com".equals(s);
                        }
                    });
                    //myConnection.setRequestProperty("User-Agent", "Pancreastic-");
                    myConnection.setRequestProperty("Content-Type", "application/json");
                    myConnection.setRequestMethod(method);
                    myConnection.setDoOutput(true);
                    myConnection.setDoInput(true);

                    final JSONObject json = new JSONObject(requestBody);

                    final OutputStreamWriter osw = new OutputStreamWriter(myConnection.getOutputStream());

                    osw.write(json.toString());
                    osw.flush();
                    final int statusCode = myConnection.getResponseCode();
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handler.response(statusCode, null);
                        }
                    });


                } catch (Exception ex) {
                    Log.e("REST", ex.toString());
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handler.response(503, null);
                        }
                    });
                }
            }
        });
    }
    public void deviceLogin(RestResultHandler handler) {
        final Map<String, String> jsonBody = new HashMap<>();
        sendRestRequestOnNewThread("devicelogin", "POST", jsonBody, handler);
    }

    public void deviceRegister(String key, RestResultHandler handler) {
        final Map<String, String> jsonBody = new HashMap<>();
        jsonBody.put("key", key);
        sendRestRequestOnNewThread("deviceregister", "POST", jsonBody, handler);
    }
}
