package com.mbealesolutions.pancreastic.event;

/**
 * Created by Mike on 01/03/2018.
 */

public enum Event {
    SensorConnected,
    SensorConnectFailed,
    SensorDisconnected,
    SensorDataReceived,
    DeviceNoNfc,
    DeviceDisabledNfc,
    AppStarted
}
