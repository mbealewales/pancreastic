package com.mbealesolutions.pancreastic.nfc ;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcV;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;

import com.mbealesolutions.pancreastic.event.Event;
import com.mbealesolutions.pancreastic.event.EventHandler;

/**
 * Created by Mike on 01/03/2018.
 */

public class NfcHandler {

    private static final long READ_PERIOD = 60000;
    private static final long READ_DELAY = 20000;

    private EventHandler eventHandler;
    private NfcAdapter nfcAdapter;
    private Tag tag;

    public NfcHandler(NfcAdapter nfcAdapter, EventHandler eventHandler) {
        this.nfcAdapter = nfcAdapter;
        this.eventHandler = eventHandler;

        if (nfcAdapter == null) {
            eventHandler.event(Event.DeviceNoNfc);
        }
    }

    public void connectToSensor(Intent nfcIntent) {
        if (nfcAdapter != null) {
            if (nfcAdapter.isEnabled()) {
                Log.d("MB:NFC", "connecting");
                String action = nfcIntent.getAction();
                if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
                    // In case we would still use the Tech Discovered Intent
                    tag = nfcIntent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                    new NfcVConnectTask().execute(tag);
                }
            } else {
                eventHandler.event(Event.DeviceDisabledNfc);
            }
        }
    }

    public void pause(Activity activity) {
        nfcAdapter.disableForegroundDispatch(activity);
    }

    public void resume(Activity activity) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);

        nfcAdapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    public void stop() {

    }

    private void startReading() {
        if (tag != null) {
            new NfcReadTask().execute(tag);
        }
    }

    private class NfcVConnectTask extends AsyncTask<Tag, Void, Event> {
        @Override
        protected Event doInBackground(Tag... tags) {
            final Tag tag = tags[0];
            final NfcV nfcvTag = NfcV.get(tag);

            try {
                nfcvTag.connect();
                Log.d("MB:NFC", "connected");
                return Event.SensorConnected;
            } catch (IOException ioe) {
                Log.d("MB:NFC", "connect failed");
                return Event.SensorConnectFailed;
            }
        }

        @Override
        protected void onPostExecute(Event event) {
            if (event == Event.SensorConnected) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startReading();
                    }
                }, READ_DELAY);
            }
            eventHandler.event(event);
        }
    }

    private class NfcReadTask extends AsyncTask<Tag, Void, Event> {
        @Override
        protected Event doInBackground(Tag... tags) {
            final Tag tag = tags[0];
            final NfcV nfcvTag = NfcV.get(tag);
            try {
                // Get system information (0x2B)
                byte[] cmd = new byte[] {
                        (byte)0x00, // Flags
                        (byte)0x2B // Command: Get system information
                };
                Log.d("MB:NFC", "reading");
                byte[] systeminfo = nfcvTag.transceive(cmd);
                Log.d("MB:NFC", "read succeeded");

            } catch (IOException ioe) {
                Log.d("MB:NFC", "read failed");
                return Event.SensorDisconnected;
            }
            return Event.SensorDataReceived;
        }

        @Override
        protected void onPostExecute(Event event) {
            if (event == Event.SensorDataReceived) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startReading();
                    }
                }, READ_PERIOD);
            }
            eventHandler.event(event);
        }
    }
}
