package com.mbealesolutions.pancreastic.rest;

import java.util.Map;

/**
 * Created by mbeale on 03/03/2018.
 */

public interface RestResultHandler {
    void response(int statusCode, Map<String, String> body);
}
