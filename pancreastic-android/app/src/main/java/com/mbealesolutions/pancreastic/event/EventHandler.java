package com.mbealesolutions.pancreastic.event;

import java.util.Map;

import com.mbealesolutions.pancreastic.event.speech.SpeechHandler;

/**
 * Created by Mike on 01/03/2018.
 */

public class EventHandler {
    private SpeechHandler speechHandler;
    public EventHandler(SpeechHandler speechHandler) {
        this.speechHandler = speechHandler;
    }
    public void event(Event event) {
        this.speechHandler.say(event);
    }

    public void stop() {
        speechHandler.stop();
    }
}
