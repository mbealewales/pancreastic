package com.mbealesolutions.pancreastic.event.speech;

/**
 * Created by Mike on 01/03/2018.
 */

public interface SpeechInitialisedListener {
    void speechInitialized(int status);
}
